//matarial-ul 쓸꺼얌
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import FastfoodIcon from "@material-ui/icons/Fastfood";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";

import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Badge from '@material-ui/core/Badge';
import Notification from '../NotificationsComponent';
import BellIcon from 'react-bell-icon';
import msgImage from '../msgImage.png';


const style = {
  flexGrow: 1
};

var stompClient = null;

class NavBar extends React.Component {

  constructor(props) {
    super(props);
    this.state =
      {
        openNotifications: false,
        anchorEl: null,
        privateMessages: [],
        isPrivateMessage: false
      };

    this.connect()
  }

  connect = () => {

    if (this.props.username) {

      const Stomp = require('stompjs')

      var SockJS = require('sockjs-client')

      SockJS = new SockJS('/ws')

      stompClient = Stomp.over(SockJS);

      stompClient.connect({}, this.onConnected, this.onError);

      this.setState({
        username: this.props.username,
      })
    }
  }

  onConnected = () => {

    // Subscribing to the private topic
    stompClient.subscribe('/user/' + this.props.username.toString().toLowerCase() + '/reply', this.onMessageReceived);
    // Registering user to server as a private chat user
    stompClient.send('/app/addPrivateUser', {}, JSON.stringify({ sender: this.props.username, type: 'JOIN' }))
  }

  sendMessage = (type, value) => {

    this.setState({
      bellRing: true
    })

    if (stompClient) {
      var chatMessage = {
        sender: this.props.user,
        content: type === 'TYPING' ? value : value,
        type: type

      };
      // send private message
      stompClient.send('/app/sendPrivateMessage', {}, JSON.stringify(chatMessage));

    }
  }

  onMessageReceived = (payload) => {

    var message = JSON.parse(payload.body);

    if (message.type === 'CHAT') {
      this.state.privateMessages.push({
        message: message.content,
        sender: message.sender,
        dateTime: message.dateTime
      })
      this.setState({
        privateMessages: this.state.privateMessages,
      })
    }
  }

  handleOpenNotifications = () => {
    this.setState({
      openNotifications: true,
      isPrivateMessage: false
    })
  }

  handleOpenPrivateMessages = () => {
    this.setState({
      openNotifications: true,
      isPrivateMessage: true
    })
  }

  handleCloseNotifications = () => {
    this.setState({
      openNotifications: false
    })
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleLogOut = () => {
    window.location.reload();
  };


  render() {
    //props 설정
    const { logged, onLogout } = this.props;

    const { auth, anchorEl } = this.state;
    const open = Boolean(anchorEl);

    const styled = {
      textAlign: "right"
    };
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <IconButton className="" color="inherit" aria-label="Menu">
              <MenuIcon onClick={this.handleClick} />
            </IconButton>
            <Typography variant="h6" style={style}>
              맛집의 민족 <FastfoodIcon />
            </Typography>
            <Link to="/main" className="Nav_Links">
              Main
            </Link>
            <Link to="/chat" className="Nav_Links">
              Chat
            </Link>
            <Link to="/users" className="Nav_Links">
              User List
            </Link>
            <Link to="/mypage" className="Nav_Links">
              My Page
            </Link>
            {logged ? (
              <div style={styled}>
                <div>
                  <Link to="/" onClick={onLogout} className="Nav_Links">
                    {" "}
                    로그아웃{" "}
                  </Link>
                </div>
              </div>
            ) : (
              <div style={styled}>
                <div>
                  <Link to="/login" className="Nav_Links">
                    {" "}
                    로그인{" "}
                  </Link>
                </div>
              </div>
            )}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default NavBar;
