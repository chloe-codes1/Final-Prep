import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: ""
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.username !== this.props.username;
  }

  handleUserNameChange = event => {
    this.setState({
      username: event.target.value
    });
  };

  //접속
  handleConnectPublicly = () => {
    this.props.connect(this.state.username, false);
    //-> 부모인 ChatMessageBox의 connect()호출
  };

  render() {
    return (
      <div className="MyModal" style={{ textAlign: "center" }}>
        <div className="content">
          <h4 style={{ fontSize: "16pt", textAlign: "center" }}>
            채팅 시작하기
          </h4>
          <p>채팅할 유저의 닉네임을 입력하세요</p>
          <div>
            <TextField
              id="user"
              label="닉네임을 입력해주세요"
              placeholder="Username"
              onChange={this.handleUserNameChange}
              style={{ width: "200px" }}
            />
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleConnectPublicly}
            >
              찾기
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
